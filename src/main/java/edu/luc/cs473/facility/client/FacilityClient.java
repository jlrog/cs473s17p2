package edu.luc.cs473.facility.client;

import edu.luc.cs473.facility.models.facility.Details;
import edu.luc.cs473.facility.models.facility.Facility;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import edu.luc.cs473.facility.service.FacilityService;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by jlroo on 3/21/17.
 */

public class FacilityClient {
    public static void main() throws Exception {

        ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
        FacilityService facilityService = (FacilityService) context.getBean("facilityService");
        Facility customer01 = facilityService.getFacility();

        customer01.setFacilityID(1);
        customer01.setRoomNumber(101);
        customer01.setMedia(true);
        customer01.setMaxCapacity(12);
        customer01.setName("paper");

        Details customerDetails01 = customer01.getDetails();

        customerDetails01.setFacilityID(customer01.getFacilityID());
        customerDetails01.setPhoneNumber("312-915-6100");
        customerDetails01.setDepartment("capital");
        customerDetails01.setOccupied(true);
        customerDetails01.setInspected(LocalDate.of(2017,01,28));
        customer01.setDetails(customerDetails01);

        facilityService.addFacility(customer01);

        /**  -----------------------------------------------------  **/

        Facility customer02 = facilityService.getFacility();

        customer02.setFacilityID(2);
        customer02.setRoomNumber(202);
        customer02.setMedia(true);
        customer02.setMaxCapacity(5);
        customer02.setName("space");

        Details customerDetails02 = customer02.getDetails();

        customerDetails02.setFacilityID(customer02.getFacilityID());
        customerDetails02.setPhoneNumber("312-915-6200");
        customerDetails02.setDepartment("finance");
        customerDetails02.setOccupied(true);
        customerDetails02.setInspected(LocalDate.of(2017,04,10));
        customer02.setDetails(customerDetails02);

        facilityService.addFacility(customer02);

        /**  -----------------------------------------------------  **/

        Facility customer03 = facilityService.getFacility();

        customer03.setFacilityID(3);
        customer03.setRoomNumber(130);
        customer03.setMedia(true);
        customer03.setMaxCapacity(8);
        customer03.setName("greenRoom");

        Details customerDetails03 = customer03.getDetails();

        customerDetails03.setFacilityID(customer03.getFacilityID());
        customerDetails03.setPhoneNumber("312-915-6300");
        customerDetails03.setDepartment("Marketing");
        customerDetails03.setOccupied(true);
        customerDetails03.setInspected(LocalDate.of(2017,03,05));
        customer03.setDetails(customerDetails03);

        facilityService.addFacility(customer03);

        /**  -----------------------------------------------------  **/

        Facility customer04 = facilityService.getFacility();

        customer04.setFacilityID(4);
        customer04.setRoomNumber(104);
        customer04.setMedia(false);
        customer04.setMaxCapacity(5);
        customer04.setName("nextRock");

        Details customerDetails04 = customer04.getDetails();

        customerDetails04.setFacilityID(customer04.getFacilityID());
        customerDetails04.setPhoneNumber("312-915-6400");
        customerDetails04.setDepartment("agriculture");
        customerDetails04.setOccupied(false);
        customerDetails04.setInspected(LocalDate.of(2017,01,05));
        customer04.setDetails(customerDetails04);

        facilityService.addFacility(customer04);
        
        List<String> facilities;
        System.out.println("\n[TEST] Facilities in the system ");
        System.out.println("-------------------------------------------------------");
        facilities = facilityService.getAllFacilities();

        for (String line: facilities){
            System.out.println( line );
        }
        System.out.println("-------------------------------------------------------");

    }
}