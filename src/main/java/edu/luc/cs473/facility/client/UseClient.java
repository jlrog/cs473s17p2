package edu.luc.cs473.facility.client;

import edu.luc.cs473.facility.models.use.FacilityUse;
import edu.luc.cs473.facility.models.use.Inspection;
import edu.luc.cs473.facility.service.UseService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by jlroo on 3/21/17.
 */
public class UseClient {
    public static void main() throws Exception {

        ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
        UseService useService = (UseService) context.getBean("useService");

        /**  -----------------------------------------------------  **/

        FacilityUse customerOrder01 = useService.getFacilityUse(); 
        
        customerOrder01.setFacilityID(1);
        customerOrder01.setCustomerID(1002003);
        customerOrder01.setReservationStart(LocalDate.of(2017, 01, 01));
        customerOrder01.setReservationEnd(LocalDate.of(2017, 01, 02));
        customerOrder01.setOccupied(true);
        
        Inspection inspection01 = customerOrder01.getInspection();

        inspection01.setInspectionDate(LocalDate.of(2017, 04, 01));
        inspection01.setInspectionCode("XO10");
        inspection01.setPassedInspection(false);
        inspection01.setDescription("bad wireless connection");
        customerOrder01.setInspection(inspection01);

        /**  -----------------------------------------------------  **/

        FacilityUse customerOrder02 = useService.getFacilityUse();

        customerOrder02.setFacilityID(2);
        customerOrder02.setCustomerID(1002004);
        customerOrder02.setReservationStart(LocalDate.of(2017, 01, 01));
        customerOrder02.setReservationEnd(LocalDate.of(2017, 01, 02));
        customerOrder02.setOccupied(true);

        Inspection inspection02 = customerOrder02.getInspection();

        inspection02.setInspectionDate(LocalDate.of(2017, 04, 01));
        inspection02.setInspectionCode("XO10");
        inspection02.setPassedInspection(true);
        inspection02.setDescription("good wireless connection");
        customerOrder02.setInspection(inspection02);

        /**  -----------------------------------------------------  **/

        FacilityUse customerOrder03 = useService.getFacilityUse();

        customerOrder03.setFacilityID(1);
        customerOrder03.setCustomerID(1002005);
        customerOrder03.setReservationStart(LocalDate.of(2017, 01, 01));
        customerOrder03.setReservationEnd(LocalDate.of(2017, 01, 02));
        customerOrder03.setOccupied(true);

        Inspection inspection03 = customerOrder03.getInspection();

        inspection03.setInspectionDate(LocalDate.of(2017, 04, 01));
        inspection03.setInspectionCode("XO10");
        inspection03.setPassedInspection(false);
        inspection03.setDescription("bad computer");
        customerOrder03.setInspection(inspection03);

        /**  -----------------------------------------------------  **/

        useService.addFacilityUse(customerOrder01);
        useService.addFacilityUse(customerOrder02);
        useService.addFacilityUse(customerOrder03);

        System.out.println("\n[TEST] Facility Usage in the system ");
        System.out.println("------------------------------------------------------- ");
        List<String> ordersAllFacilities;
        ordersAllFacilities = useService.getAllOrders();
        for (String line : ordersAllFacilities) {System.out.println(line);}
        System.out.println("------------------------------------------------------- ");

    }
}
