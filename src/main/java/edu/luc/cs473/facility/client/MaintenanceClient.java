package edu.luc.cs473.facility.client;

import edu.luc.cs473.facility.models.maintenance.MaintenanceImpl;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import edu.luc.cs473.facility.service.MaintenanceService;
import edu.luc.cs473.facility.models.maintenance.Maintenance;


import java.time.LocalDate;
import java.util.List;

/**
 * Created by jlroo on 3/21/17.
 */
public class MaintenanceClient {
    
    public static void main() throws Exception {
        
        ApplicationContext context = new ClassPathXmlApplicationContext("META-INF/app-context.xml");
        MaintenanceService maintenanceService = (MaintenanceService) context.getBean("maintenanceService");
        
        Maintenance maintenance01 = maintenanceService.getMaintenance();
        
        maintenance01.setFacilityID(1);
        maintenance01.setMaintenanceStart(LocalDate.of(2017, 01, 01));
        maintenance01.setMaintenanceEnd(LocalDate.of(2017, 01, 05));
        maintenance01.setFacilityDowntime(maintenance01.getMaintenanceStart(), maintenance01.getMaintenanceEnd());
        maintenance01.setMaintenanceCost(2500);
        maintenance01.setMaintenanceLog("AC damaged, TV not working, computer broken");
        
        maintenanceService.addMaintenance(maintenance01);

        /**  -----------------------------------------------------  **/

        Maintenance maintenance02 = maintenanceService.getMaintenance();
        maintenance02.setFacilityID(2);
        maintenance02.setMaintenanceStart(LocalDate.of(2017, 01, 05));
        maintenance02.setMaintenanceEnd(LocalDate.of(2017, 01, 15));
        maintenance02.setFacilityDowntime(maintenance02.getMaintenanceStart(), maintenance02.getMaintenanceEnd());
        maintenance02.setMaintenanceCost(2000);
        maintenance02.setMaintenanceLog("Computer not working");
        
        maintenanceService.addMaintenance(maintenance02);

        /**  -----------------------------------------------------  **/


        Maintenance maintenance03;
        maintenance03 = maintenanceService.getMaintenance();
        maintenance03.setFacilityID(3);
        maintenance03.setMaintenanceStart(LocalDate.of(2017, 03, 10));
        maintenance03.setMaintenanceEnd(LocalDate.of(2017, 04, 22));
        maintenance03.setFacilityDowntime(maintenance03.getMaintenanceStart(), maintenance03.getMaintenanceEnd());
        maintenance03.setMaintenanceCost(3000);
        maintenance03.setMaintenanceLog("water problem");

        maintenanceService.addMaintenance(maintenance03);

        /**  -----------------------------------------------------  **/

        Maintenance maintenance04 = maintenanceService.getMaintenance();
        maintenance04.setFacilityID(1);
        maintenance04.setMaintenanceStart(LocalDate.of(2016, 12, 10));
        maintenance04.setMaintenanceEnd(LocalDate.of(2016, 12, 12));
        maintenance04.setFacilityDowntime(maintenance04.getMaintenanceStart(), maintenance04.getMaintenanceEnd());
        maintenance04.setMaintenanceCost(3000);
        maintenance04.setMaintenanceLog("electrical problem");

        maintenanceService.addMaintenance(maintenance04);

        /**  -----------------------------------------------------  **/

        System.out.println("\n[TEST] Maintenance in the system ");
        List<String> maintenance_facilities;
        System.out.println("-------------------------------------------------------");
        maintenance_facilities = maintenanceService.getAllMaintenance();
        for (String line : maintenance_facilities) {System.out.println(line);}
        System.out.println("------------------------------------------------------- ");
    }
}
