package edu.luc.cs473.facility.client;

import java.util.Scanner;

/**
 * Created by jlroo on 3/21/17.
 */

public class MainClient {

    public static void main(String[] args) throws Exception {

        boolean flag = false;

        do {
            System.out.println("\n Select an option from the menu:");
            System.out.println("\n \t 1) Spring Facility Client");
            System.out.println("\n \t 2) Spring Use Client");
            System.out.println("\n \t 3) Spring Maintenance Client");
            System.out.println("\n \t 4) Quit");

            Scanner scanner = new Scanner(System.in);
            int option = Integer.parseInt(scanner.next());

            switch(option) {
                case 1:
                    FacilityClient facilityClient = new FacilityClient();
                    facilityClient.main();
                    break;
                case 2:
                    UseClient useClient = new UseClient();
                    useClient.main();
                    break;
                case 3:
                    MaintenanceClient maintenance = new MaintenanceClient();
                    maintenance.main();
                    break;
                case 4:
                    flag = true;
                    break;
                default:
                    System.out.println("Invalid option.");
                    break;
            }
        }while (flag==false);
    }
}
