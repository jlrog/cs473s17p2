package edu.luc.cs473.facility.models.use;

import lombok.Data;
import java.time.LocalDate;

/**
 * Created by jlroo on 2/20/17.
 */

@Data
public class FacilityUseImpl implements FacilityUse {

    private int facilityID;
    private int orderNumber;
    private LocalDate reservationStart;
    private LocalDate reservationEnd;
    private boolean occupied;
    private int customerID;
    private Inspection inspection;

    public Inspection getInspection() {
        return inspection;
    }
    public void setInspection(Inspection inspection) {
        this.inspection = inspection;
    }
}
