package edu.luc.cs473.facility.models.use;
import lombok.Data;

import java.time.LocalDate;

/**
 * Created by jlroo on 2/20/17.
 */

@Data
public class InspectionImpl implements Inspection {
    private String inspectionCode;
    private LocalDate inspectionDate;
    private Boolean passedInspection;
    private String description;
}
