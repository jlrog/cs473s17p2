package edu.luc.cs473.facility.models.facility;

import lombok.Data;

@Data
public class FacilityImpl implements Facility {
    private int facilityID;
    private String name;
    private int roomNumber;
    private boolean media;
    private int maxCapacity;
    private Details details;

    public Details getDetails() {
        return details;
    }
    public void setDetails(Details details) {
        this.details = details;
    }
}
