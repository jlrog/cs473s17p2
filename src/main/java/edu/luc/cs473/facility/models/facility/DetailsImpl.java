package edu.luc.cs473.facility.models.facility;

import lombok.Data;

import java.time.LocalDate;

/**
 * Created by jlroo on 2/20/17.
 */

@Data
public class DetailsImpl implements Details {

    private int facilityID;
    private String phoneNumber;
    private String department;
    private boolean occupied;
    private LocalDate inspected;
    private Details details;

    public Details getDetails() {
        return details;
    }
    public void setDetails(Details details) {this.details = details;}
}
