package edu.luc.cs473.facility.service;

import edu.luc.cs473.facility.dal.UseDAO;
import edu.luc.cs473.facility.models.use.FacilityUse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by jlroo on 2/20/17.
 */

public class UseService {

    private UseDAO _useDAO = new UseDAO();
    private FacilityUse facility;

    public void setFacilityUse(FacilityUse facility) {
        this.facility = facility;
    }
    public FacilityUse getFacilityUse() {
        return facility;
    }

    public List<String> getAllOrders() {
        List<String> orders = new ArrayList<>();
        try {
            Collection<FacilityUse> AllOrders = _useDAO.ReadAllOrders();
            for (FacilityUse entry:AllOrders) {
                
                String response =   "facility_id: " + entry.getFacilityID() +
                                    " order_number: "+ entry.getOrderNumber() +
                                    " customer_id: "+ entry.getCustomerID() +
                                    "\nreservation_start: "+ entry.getReservationStart() +
                                    " reservation_end: "+ entry.getReservationEnd() +
                                    "\ninspection_date: "+ entry.getInspection().getInspectionDate();
                orders.add(response);
            }

        } catch (Exception se) {
            System.err.println(se.getMessage());
        }
        return orders;
    }

    public String getFacilityOrder(int facilityID) {
        String response = "";
        try {
            FacilityUse facility = _useDAO.ReadFacilityOrders(facilityID);

            response =  "facility_id: " + facility.getFacilityID() +
                        " order_number: "+ facility.getOrderNumber() +
                        " customer_id: "+ facility.getCustomerID() +
                        "\nreservation_start: "+ facility.getReservationStart() +
                        " reservation_end: "+ facility.getReservationEnd() +
                        "\ninspection_date: "+ facility.getInspection().getInspectionDate();

        } catch(Exception se) {
            System.err.println(se.getMessage());
        }
        return response;
    }

    public String addFacilityUse(FacilityUse newObj) {
        try { _useDAO.Insert(newObj); }
        catch(Exception se) {
            System.err.println(se.getMessage());
        }
        return "success";
    }

    public String updateFacilityUse(FacilityUse FacilityUseObj, int facilityID) {
        try { _useDAO.Update(FacilityUseObj,facilityID); }
        catch(Exception se) { System.err.println(se.getMessage()); }
        return "success";
    }

    public String deleteFacilityUse(int deleteID) {
        try { _useDAO.Delete(deleteID); }
        catch(Exception se) { System.err.println(se.getMessage()); }
        return "success";
    }
    
}