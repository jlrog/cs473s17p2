package test.view;

import edu.luc.cs473.facility.models.use.FacilityUseImpl;
import edu.luc.cs473.facility.models.use.InspectionImpl;
import edu.luc.cs473.facility.service.UseService;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by jlroo on 2/20/17.
 */

public class testUseClient {

    public testUseClient() throws Exception {

        UseService UseFacilityService = new UseService();

        FacilityUseImpl customerOrder01 = new FacilityUseImpl();
        customerOrder01.setFacilityID(1);
        customerOrder01.setCustomerID(1002003);
        customerOrder01.setReservationStart(LocalDate.of(2017, 01, 01));
        customerOrder01.setReservationEnd(LocalDate.of(2017, 01, 02));
        customerOrder01.setOccupied(true);
        InspectionImpl inspection01 = new InspectionImpl();
        inspection01.setInspectionDate(LocalDate.of(2017, 04, 01));
        inspection01.setInspectionCode("XO10");
        inspection01.setPassedInspection(false);
        inspection01.setDescription("bad wireless connection");
        customerOrder01.setInspection(inspection01);

        FacilityUseImpl customerOrder02 = new FacilityUseImpl();
        customerOrder02.setFacilityID(2);
        customerOrder02.setCustomerID(1002004);
        customerOrder02.setReservationStart(LocalDate.of(2017, 01, 01));
        customerOrder02.setReservationEnd(LocalDate.of(2017, 01, 02));
        customerOrder02.setOccupied(true);
        InspectionImpl inspection02 = new InspectionImpl();
        inspection02.setInspectionDate(LocalDate.of(2017, 04, 01));
        inspection02.setInspectionCode("XO10");
        inspection02.setPassedInspection(true);
        inspection02.setDescription("good wireless connection");
        customerOrder02.setInspection(inspection02);

        FacilityUseImpl customerOrder03 = new FacilityUseImpl();
        customerOrder03.setFacilityID(1);
        customerOrder03.setCustomerID(1002005);
        customerOrder03.setReservationStart(LocalDate.of(2017, 01, 01));
        customerOrder03.setReservationEnd(LocalDate.of(2017, 01, 02));
        customerOrder03.setOccupied(true);
        InspectionImpl inspection03 = new InspectionImpl();
        inspection03.setInspectionDate(LocalDate.of(2017, 04, 01));
        inspection03.setInspectionCode("XO10");
        inspection03.setPassedInspection(false);
        inspection03.setDescription("bad computer");
        customerOrder03.setInspection(inspection03);

        UseFacilityService.addFacilityUse(customerOrder01);
        UseFacilityService.addFacilityUse(customerOrder02);
        UseFacilityService.addFacilityUse(customerOrder03);

        System.out.println("\n[TEST] Facility Usage in the system ");
        System.out.println("------------------------------------------------------- ");
        List<String> ordersAllFacilities;
        ordersAllFacilities = UseFacilityService.getAllOrders();
        for (String line : ordersAllFacilities) {
            System.out.println(line);
        }
        System.out.println("------------------------------------------------------- ");

        System.out.println("\n[TEST] Facility Usage Get Facility Order 01 ");
        String ordersFacility = UseFacilityService.getFacilityOrder(1);
        System.out.print(ordersFacility+"");

        System.out.println("\n[TEST] Facility Usage Delete Facility Order 02 ");
        UseFacilityService.deleteFacilityUse(2);

        System.out.println("[TEST] Facility Usage in the system ");
        System.out.println("------------------------------------------------------- ");

        ordersAllFacilities = UseFacilityService.getAllOrders();
        for (String line: ordersAllFacilities){
            System.out.println( line );
        }
        System.out.println("------------------------------------------------------- ");

        System.out.println("[TEST] Facility Usage: Add and Update Facility 01 ");

        customerOrder01.setReservationStart(LocalDate.of(2017,3,10));
        customerOrder01.setReservationEnd(LocalDate.of(2017,03,15));
        customerOrder01.setOccupied(false);
        inspection01.setInspectionDate(LocalDate.of(2017,02,01));
        inspection01.setInspectionCode("XO11");
        inspection01.setPassedInspection(false);
        inspection01.setDescription("fail inspection");

        UseFacilityService.updateFacilityUse(customerOrder01,1);
        String readUse = UseFacilityService.getFacilityOrder(1);
        System.out.print(readUse);

        System.out.println("\n[TEST] Facility Usage in the system ");
        System.out.println("------------------------------------------------------- ");
        ordersAllFacilities = UseFacilityService.getAllOrders();
        for (String line: ordersAllFacilities){
            System.out.println( line );
        }
        System.out.println("------------------------------------------------------- ");

    }
}
