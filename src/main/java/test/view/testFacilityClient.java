package test.view;

import edu.luc.cs473.facility.models.facility.DetailsImpl;
import edu.luc.cs473.facility.models.facility.Facility;
import edu.luc.cs473.facility.models.facility.Details;
import edu.luc.cs473.facility.models.facility.FacilityImpl;
import edu.luc.cs473.facility.service.FacilityService;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by jlroo on 2/20/17.
 */

public class testFacilityClient {

    public testFacilityClient() throws Exception {

        FacilityService facilityService = new FacilityService();

        //set up facilities for dummy data
        Facility studyRoom01 = new FacilityImpl() ;
        Details studyDetails01 = new DetailsImpl();
        studyRoom01.setFacilityID(1);
        studyRoom01.setRoomNumber(101);
        studyRoom01.setMedia(false);
        studyRoom01.setMaxCapacity(10);
        studyRoom01.setName("blackRock");
        studyDetails01.setFacilityID(studyRoom01.getFacilityID());
        studyDetails01.setPhoneNumber("312-915-6300");
        studyDetails01.setDepartment("Marketing");
        studyDetails01.setOccupied(true);
        studyDetails01.setInspected(LocalDate.of(2017,02,05));
        studyRoom01.setDetails(studyDetails01);

        System.out.println("[TEST] Facility Client: Add Facility 01 ");
        facilityService.addFacility(studyRoom01);

        Facility studyRoom02 = new FacilityImpl() ;
        Details studyDetails02 = new DetailsImpl();
        studyRoom02.setFacilityID(2);
        studyRoom02.setRoomNumber(102);
        studyRoom02.setMedia(false);
        studyRoom02.setMaxCapacity(5);
        studyRoom02.setName("rock");
        studyDetails02.setFacilityID(studyRoom02.getFacilityID());
        studyDetails02.setPhoneNumber("312-915-6200");
        studyDetails02.setDepartment("finance");
        studyDetails02.setOccupied(true);
        studyDetails02.setInspected(LocalDate.of(2017,01,10));
        studyRoom02.setDetails(studyDetails02);

        System.out.println("[TEST] Facility Client: Add Facility 02 ");
        facilityService.addFacility(studyRoom02);

        Facility studyRoom03 = new FacilityImpl() ;
        Details studyDetails03 = new DetailsImpl();
        studyRoom03.setFacilityID(3);
        studyRoom03.setRoomNumber(103);
        studyRoom03.setMedia(false);
        studyRoom03.setMaxCapacity(5);
        studyRoom03.setName("center");
        studyDetails03.setFacilityID(studyRoom03.getFacilityID());
        studyDetails03.setPhoneNumber("312-915-6500");
        studyDetails03.setDepartment("agriculture");
        studyDetails03.setOccupied(true);
        studyDetails03.setInspected(LocalDate.of(2017,02,10));
        studyRoom03.setDetails(studyDetails03);

        System.out.println("[TEST] Facility Client: Add Facility 03 ");
        facilityService.addFacility(studyRoom03);

        Facility studyRoom04 = new FacilityImpl() ;
        Details studyDetails04 = new DetailsImpl();
        studyRoom04.setFacilityID(4);
        studyRoom04.setRoomNumber(104);
        studyRoom04.setMedia(true);
        studyRoom04.setMaxCapacity(15);
        studyRoom04.setName("mainRoom");
        studyDetails04.setFacilityID(studyRoom04.getFacilityID());
        studyDetails04.setPhoneNumber("312-915-6600");
        studyDetails04.setDepartment("new age");
        studyDetails04.setOccupied(true);
        studyDetails04.setInspected(LocalDate.of(2017,04,10));
        studyRoom04.setDetails(studyDetails04);

        System.out.println("[TEST] Facility Client: Add Facility 04 ");
        facilityService.addFacility(studyRoom04);

        List <String> facilities;
        System.out.println("[TEST] Facilities in the system ");
        System.out.println("-------------------------------------------------------");
        facilities = facilityService.getAllFacilities();
        for (String line: facilities){
            System.out.println( line );
        }
        System.out.println("-------------------------------------------------------");

        System.out.println("[TEST] Facility Client: Get individual Facility 01 ");
        String readFacility = facilityService.getFacility(1);
        System.out.print(readFacility);

        System.out.println("\n[TEST] Facility Client: Add and Update a Facility ");

        studyRoom03.setName("Global");
        studyRoom03.setRoomNumber(1010);
        studyRoom03.setMedia(true);
        studyDetails03.setPhoneNumber("312-915-6088");
        studyDetails03.setOccupied(false);
        studyDetails03.setInspected(LocalDate.of(2017,03,10));
        studyRoom03.setDetails(studyDetails03);
        facilityService.updateFacility(studyRoom03,3);

        System.out.println("[TEST] Facility Client: Get individual Facility 03 ");
        String readFacility2 = facilityService.getFacility(3);
        System.out.print(readFacility2);

        System.out.println("\n[TEST] Facility Client: Delete Facility 03 ");
        facilityService.deleteFacility(4);

        System.out.println("[TEST] Facilities in the system ");
        System.out.println("-------------------------------------------------------");
        facilities = facilityService.getAllFacilities();
        for (String line: facilities){
            System.out.println( line );
        }
        System.out.println("------------------------------------------------------- ");

    }
}